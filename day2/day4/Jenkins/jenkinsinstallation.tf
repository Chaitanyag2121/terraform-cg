provider "aws" {
    region = "ap-southeast-2"
}

resource "aws_instance" "jenkins_instance" {
  ami           = "ami-04f5097681773b989"
  instance_type = "t2.micro"
  key_name      = "sydney"
  vpc_security_group_ids = ["sg-00a0440feeb33f34d"]
  tags = {
    Name = "jenkins_instance"
  }
    user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update -y
              sudo dnf install fontconfig java-17-openjdk
              sudo wget -O /etc/yum.repos.d/jenkins.repo \
                     https://pkg.jenkins.io/redhat-stable/jenkins.repo
              sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io-2023.key
              sudo dnf upgrade
              sudo dnf install jenkins             
              sudo systemctl daemon-reload 
              sudo systemctl enable jenkins
              sudo systemctl start jenkins
              EOF
}
