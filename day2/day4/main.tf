provider "aws" {
    region = "ap-southeast-2"
}

terraform {
    backend "s3" {
        region = "ap-southeast-2"
        bucket = "practic-ebucket"
        key = "./terraform.tfstate"
    }
}

data "aws_security_group" "existing_sg" {
    name = "default"
    vpc_id = "vpc-0d63ae6286b65f6a0"
}

resource "aws_security_group_rule" "allow_ssh" {
    type            = "ingress"
    to_port         = 22
    protocol        = "TCP"
    from_port       = 22
    cidr_blocks     = ["0.0.0.0/0"]
    vpc_security_group_id = data.aws_security_group.existing_sg.id
}

resource "aws_security_group_rule" "allow_http" {
    type            = "ingress"
    to_port         = 80
    protocol        = "TCP"
    from_port       = 80
    cidr_blocks     = ["0.0.0.0/0"]
    vpc_security_group_id = data.aws_security_group.existing_sg.id
}

resource "aws_instance" "my_instance" {
    ami = "ami-04f5097681773b989"
    instance_type = "t2.micro"
    key_name = "sydney"
    vpc_security_group_ids = [data.aws_security_group.existing_sg.id]

    connection {
        type        = "ssh"
        user        = "ec2-user"
        private_key = file("./private.pem")
        host        = "self.public_ip"

    }
}

