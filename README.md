## Terraform

## What is Terraform?
1.] Terraform is an infrastructure as code tool that lets you build, change, and version cloud and on-prem resources safely and efficiently.
2.] HashiCorp Terraform is an infrastructure as code tool that lets you define both cloud and on-prem resources in human-readable configuration files that you can version, reuse, and share. 
3.] You can then use a consistent workflow to provision and manage all of your infrastructure throughout its lifecycle. 
4.] Terraform can manage low-level components like compute, storage, and networking resources, as well as high-level components like DNS entries and SaaS features.

## Why Terraform?
1.] Terraform allows you to describe your complete infrastructure in the form of code. 
2.] Even if your servers come from different providers such as AWS or Azure, Terraform helps you build and manage these resources in parallel across providers.
3.] Terraform lets you both deploy a Kubernetes cluster and manage its resources (e.g., pods, deployments, services, etc.). 
4.] You can also use the Kubernetes Operator for Terraform to manage cloud and on-prem infrastructure through a Kubernetes Custom Resource Definition (CRD) and Terraform Cloud.
5.] HashiCorp Terraform is an infrastructure as code (IaC) software tool that allows DevOps teams to automate infrastructure provisioning using reusable, shareable, human-readable configuration files. The tool can automate infrastructure provisioning in both on-premises and cloud environments.

## Terraform Language

There are two types of languages:
1.] .tf / .tf.json
2.] HCL / JSON
However, the configuration files used in Terraform are written in its own declarative language called HashiCorp Configuration Language (HCL), which is designed to be easy to read and write, and provides a simple syntax for defining infrastructure resources and their dependencies

## Terraform Blocks

provider
terraform
resource
data
output
variable
module


## Terraform lifecycle

init
plan
apply
destroy


## Dependencies

Implicit
Explicit